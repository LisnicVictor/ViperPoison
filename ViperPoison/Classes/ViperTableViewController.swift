//
//  ViperTableViewController.swift
//  Pods
//
//  Created by Victor Lisnic on 7/27/17.
//
//

import Foundation
import UIKit

open class ViperTableViewController : UITableViewController, ViperModuleTransitionHandler {

    open var moduleInput: ViperModuleInput? {
        return nil
    }

    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handle(segue: segue, wrapper: sender)
    }
}
