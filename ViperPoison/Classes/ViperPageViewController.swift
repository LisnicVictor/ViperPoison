//
//  ViperPageViewController.swift
//  Pods-ViperPoison_Tests
//
//  Created by Victor Lisnic on 3/12/18.
//

import UIKit

open class ViperPageViewController: UIPageViewController, ViperModuleTransitionHandler {
    open var moduleInput: ViperModuleInput? {
        return nil
    }

    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handle(segue: segue, wrapper: sender)
    }
}
