//
//  ViperViewController.swift
//  MeetNShag
//
//  Created by Victor Lisnic on 7/12/17.
//  Copyright © 2017 Vader. All rights reserved.
//

import Foundation
import UIKit

open class ViperViewController : UIViewController, ViperModuleTransitionHandler {

    open var moduleInput: ViperModuleInput? {
        return nil
    }

    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handle(segue: segue, wrapper: sender)
    }
}
