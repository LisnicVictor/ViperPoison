# ViperPoison

[![CI Status](http://img.shields.io/travis/Lisnic_Victor/ViperPoison.svg?style=flat)](https://travis-ci.org/Lisnic_Victor/ViperPoison)
[![Version](https://img.shields.io/cocoapods/v/ViperPoison.svg?style=flat)](http://cocoapods.org/pods/ViperPoison)
[![License](https://img.shields.io/cocoapods/l/ViperPoison.svg?style=flat)](http://cocoapods.org/pods/ViperPoison)
[![Platform](https://img.shields.io/cocoapods/p/ViperPoison.svg?style=flat)](http://cocoapods.org/pods/ViperPoison)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ViperPoison is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ViperPoison"
```

## Author

Lisnic_Victor, gerasim.kisslin@gmail.com

## License

ViperPoison is available under the MIT license. See the LICENSE file for more info.
