//
//  ViperCollectionViewController.swift
//  Pods
//
//  Created by Victor Lisnic on 7/27/17.
//
//

import UIKit

open class ViperCollectionViewController: UICollectionViewController , ViperModuleTransitionHandler {
    open var moduleInput: ViperModuleInput? {
        return nil
    }

    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handle(segue: segue, wrapper: sender)
    }
}
