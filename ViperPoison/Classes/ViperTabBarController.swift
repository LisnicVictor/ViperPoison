//
//  ViperTabBarController.swift
//  Pods-ViperPoison_Tests
//
//  Created by Victor Lisnic on 1/3/18.
//

import UIKit

open class ViperTabBarController: UITabBarController, ViperModuleTransitionHandler {
    open var moduleInput: ViperModuleInput? {
        return nil
    }

    open override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        handle(segue: segue, wrapper: sender)
    }
}
