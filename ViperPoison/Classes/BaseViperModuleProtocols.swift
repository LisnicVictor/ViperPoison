//
//  BaseViperModuleProtocols.swift
//  MeetNShag
//
//  Created by Victor Lisnic on 7/11/17.
//  Copyright © 2017 Vader. All rights reserved.
//

import Foundation
import UIKit


public protocol ViperModuleInput : class {
    func setOutput(output:ViperModuleOutput)
}

public extension ViperModuleInput {
    public func setOutput(output:ViperModuleOutput) {}
}

public protocol ViperModuleOutput : class {}

public protocol ViperModuleConfigurator {
    func configure(with viewInput:UIViewController)
}
